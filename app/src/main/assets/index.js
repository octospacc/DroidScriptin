//Android.ReadSharedPref = function(args){
//    return JSON.parse(
//        //decodeURIComponent(
//            Android.ReadSharedPrefJSI(
//                args.key, JSON.stringify({ value: args.default })
//            )
//        //)
//    ).value;
//};

Android.ReadSharedPref = function(key, fallback){
    return JSON.parse(
        Android.ReadSharedPrefJSI(
            key, JSON.stringify({ value: fallback })
        )
    ).value;
};

Android.WriteSharedPref = function(key, value){
    Android.WriteSharedPrefJSI(
        key, JSON.stringify({ value: value })
    );
};

Android.onEvent = function(event){};
Android.onSmsReceived = function(message, sender){};

window.onload = function(){
    try {
        eval(Android.ReadSharedPref("editScript", ""));
    } catch(e) {
        document.body.querySelector('p#log').innerHTML += '<br/>' + e;
    }
};